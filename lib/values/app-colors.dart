import 'package:flutter/material.dart';

class AppColor {
  static const Color primaryColor = Color(0xffb4a7d6);
  static const Color darkGray = Color(0xff313538);
  static const Color purple = Color(0xff674ea7);
}
