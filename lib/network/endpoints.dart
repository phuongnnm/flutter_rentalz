class APIEndpoint {
  static String searchForPropByKeyWord = """
    query searchForPropByKeyWord(\$keyword: String) {
        searchForPropByKeyWord(keyword: \$keyword){
          id
          type
          reporterName
          furnishStatus
          createdAt
        }
    }
""";

  static String getAllProps = """
    query getAllProperties {
        getAllProperties {
          id
          type
          reporterName
          createdAt
        }
    }
""";

  static String updateProperty = """
    mutation updateProperty(\$id: ID,\$type: PropertyType, \$note: String, \$furnishStatus:FurnishStatus, \$rooms: [RoomInput], \$price: Float) {
        updateProperty(id: \$id, type: \$type, note: \$note, furnishStatus: \$furnishStatus, rooms: \$rooms, price: \$price){
          id
        }
    }
""";

  static String createProperty = """
    mutation createProperty(\$type: PropertyType, \$reporterName: String, \$furnishStatus:FurnishStatus) {
        createProperty(type: \$type, reporterName: \$reporterName, furnishStatus: \$furnishStatus){
          id
        }
    }
""";

  static String registerProperty = """
    mutation registerProperty(\$input: PropertyInput) {
      registerProperty(input: \$input) {
        id
      }
    }
    """;

  static String deleteProperty = """
    mutation deleteProperty(\$id: ID!) {
      deleteProperty(id: \$id) {
        id
      }
    }
    """;
  static String getPropById = """
    query getPropById(\$id: ID!) {
      getPropById(id: \$id) {
        id
        type
        price
        reporterName
        note
        createdAt
        furnishStatus
        rooms {
          id
          name
          quantity
        }
      }
    }
    """;
}
