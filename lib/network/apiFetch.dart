class Service {
  int id;

  Service(this.id);

  factory Service.toObject(
    Map<String, dynamic> json,
  ) =>
      Service(
        json['service']['id'],
      );
}
