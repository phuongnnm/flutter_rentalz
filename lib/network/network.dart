import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:graphql/client.dart';

class GraphQLClass {
  static GraphQLClass shared = GraphQLClass();
  static HttpLink httpLink = HttpLink("http://localhost:3000/graphql");
  static Link _link = httpLink;

  final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(

      /// **NOTE** The default store is the InMemoryStore, which does NOT persist to disk
      GraphQLClient(
    cache: GraphQLCache(),
    link: _link,
  ));

  GraphQLClient clientToQuery() {
    return GraphQLClient(link: _link, cache: GraphQLCache());
  }
}
