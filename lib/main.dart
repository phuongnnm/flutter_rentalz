import 'package:flutter/material.dart';
import 'home_page/landingPage.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'network/network.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // GraphQLClass graphql = new GraphQLClass();
  var graphql = new GraphQLClass();
  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
        client: graphql.client,
        child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: MaterialApp(
              title: 'Rental Z',
              theme: ThemeData(
                primarySwatch: Colors.purple,
              ),
              home: LandingPage(),
            )));
  }
}
