class Room {
  String? name;
  int? quantity;
  String? id;

  Room({this.name, this.quantity, this.id});

  Map toJson() {
    return {'name': name, 'quantity': quantity, 'id': id};
  }
}
