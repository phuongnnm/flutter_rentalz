class PropertyInfo {
  String reporterName;
  // DateTime createdAt;
  String propertyType;
  String? note;
  String id;
  String status;
  String createdAt;

  PropertyInfo(
      {required this.reporterName,
      required this.propertyType,
      this.note,
      required this.id,
      required this.status,
      required this.createdAt});
}

enum PropertyType { MANSION, BUNGALOW, HOUSE }
