import 'package:flutter/material.dart';
import 'package:flutter_rentalz/values/app-colors.dart';
import 'formPage.dart';
import 'homepage.dart';

class LandingPage extends StatelessWidget {
  const LandingPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColor.purple,
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              Expanded(
                  child: Container(
                alignment: Alignment.center,
                child: Text(
                  "Rental Z",
                  style: TextStyle(fontSize: 40, color: Colors.white),
                ),
              )),
              Expanded(
                  child: Padding(
                      padding: EdgeInsets.only(top: 110, bottom: 110),
                      child: Column(
                        children: [
                          Container(
                              width: 200,
                              height: 50,
                              child: RawMaterialButton(
                                padding: EdgeInsets.all(10),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(12.0))),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => SearchPage()));
                                },
                                fillColor: AppColor.primaryColor,
                                child: Text("Go to search page"),
                              )),
                          Container(
                            height: 25,
                          ),
                          Container(
                              width: 200,
                              height: 50,
                              child: RawMaterialButton(
                                padding: EdgeInsets.all(10),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(12.0))),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => CustomForm()));
                                },
                                fillColor: AppColor.primaryColor,
                                child: Text("Register your property"),
                              ))
                        ],
                      )))
            ],
          ),
        ));
  }
}
