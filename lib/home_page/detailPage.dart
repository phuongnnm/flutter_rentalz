import 'package:flutter/material.dart';
import 'package:flutter_rentalz/home_page/formPage.dart';
import 'package:flutter_rentalz/models/property-info.dart';
import 'package:flutter_rentalz/models/room.dart';
import 'package:flutter_rentalz/network/endpoints.dart';
import 'package:flutter_rentalz/network/network.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:graphql/client.dart';

import '../util.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key, required this.id}) : super(key: key);
  final String id;
  @override
  _DetailPageState createState() => _DetailPageState(id);
}

class _DetailPageState extends State<DetailPage> {
  final String id;
  Map<String, bool> values = {
    'foo': false,
    'bar': false,
  };
  final _formKey = GlobalKey<FormState>();
  late bool isChecked = false;

  PropertyInfo propInfo = new PropertyInfo(
      reporterName: "loading",
      propertyType: "sth",
      id: "st",
      status: "",
      createdAt: "");

  List arr = [
    TypeCheckValue(key: 'MANSION', value: false),
    TypeCheckValue(key: 'HOUSE', value: false),
    TypeCheckValue(key: 'BUNGALOW', value: false)
  ];
  String note = "";
  String? type;
  String? status;
  String price = "";
  int numOfRows = 1;
  double roomListHeight = 80;
  List rooms = [];
  List statuses = [
    TypeCheckValue(key: 'FURNISHED', value: false),
    TypeCheckValue(key: 'UNFURNISHED', value: false),
    TypeCheckValue(key: 'PARTFURNISHED', value: false)
  ];

  _DetailPageState(this.id);

  Future<void> _showMyDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> submit() async {
    print(rooms);
    print(status);
    if (type != null && status != null && rooms.length > 0) {
      double iPrice = double.parse(price);
      print(iPrice);
      var a = await GraphQLClass.shared.clientToQuery().mutate(MutationOptions(
              document: gql(APIEndpoint.updateProperty),
              variables: {
                'id': id,
                'furnishStatus': status,
                'type': type,
                'note': note,
                'rooms': rooms,
                'price': iPrice
              }));
      print(a);
    }
  }

  @override
  void initState() {
    super.initState();
    print("INITSTATE");
    print(id);
    GraphQLClass.shared
        .clientToQuery()
        .query(QueryOptions(
            document: gql(APIEndpoint.getPropById), variables: {'id': id}))
        .then((value) {
      print(value);
      // var a = value.data!['getAllProperties'];
      var res = value.data!['getPropById'];
      type = res['type'];
      status = res['furnishStatus'];
      var date = Util.convertDate(res['createdAt']);
      price = res['price'].toString();
      note = res['note'];
      PropertyInfo e = PropertyInfo(
          reporterName: res['reporterName'],
          propertyType: res['type'],
          id: res['id'],
          status: res['furnishStatus'],
          createdAt: date);

      List roomDatas = res['rooms'];
      List roomsF = [];
      roomDatas.forEach((element) {
        Room r = Room(name: element['name'], quantity: element['quantity']);
        roomsF.add(r);
      });

      setState(() {
        rooms = roomsF;
        numOfRows = rooms.length;
        roomListHeight = (80 * numOfRows).toDouble();
      });

      print(rooms);
      arr.forEach((element) {
        if (e.propertyType == element.key) {
          setState(() {
            element.value = true;
            propInfo = e;
          });
        }
      });

      statuses.forEach((element) {
        if (e.status == element.key) {
          setState(() {
            element.value = true;
          });
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Detail"),
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pop(context);
                },
              );
            },
          ),
        ),
        body: Form(
          key: _formKey,
          child: Container(
            child: SingleChildScrollView(
                child: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          "Reporter Name:",
                          style: TextStyle(fontSize: 22),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          propInfo.reporterName,
                          style: TextStyle(
                              color: Colors.grey.shade700, fontSize: 21),
                        ),
                      )
                    ],
                  ),
                  Container(height: 30),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          "Created date:",
                          style: TextStyle(fontSize: 22),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          propInfo.createdAt,
                          style: TextStyle(
                              color: Colors.grey.shade700, fontSize: 21),
                        ),
                      )
                    ],
                  ),
                  Container(height: 30),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          width: 80,
                          child: Text(
                            "Rent price:",
                            style: TextStyle(fontSize: 22),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          controller: TextEditingController()..text = price,
                          onChanged: (text) {
                            price = text;
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Required field';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                          ),
                        ),
                      ),
                      Expanded(
                          child: Container(
                              width: 30,
                              child: Text(
                                ' VND',
                                style: TextStyle(
                                    color: Colors.grey.shade700, fontSize: 21),
                              )))
                    ],
                  ),
                  Container(height: 30),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Property Type:",
                      style: TextStyle(fontSize: 22),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 10),
                    height: 200,
                    child: Scrollbar(
                        child: ListView.builder(
                            padding: const EdgeInsets.only(top: 10),
                            itemCount: arr.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                child: CheckboxListTile(
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    title: Text(arr[index].key),
                                    value: arr[index].value,
                                    onChanged: (val) {
                                      setState(() {
                                        arr.forEach((element) {
                                          element.value = false;
                                        });

                                        if (val != null) {
                                          arr[index]!.value = val;
                                          type = arr[index]!.key;
                                        }
                                      });
                                    }),
                              );
                            })),
                  ),
                  Container(
                    height: 20,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Furniture type:",
                      style: TextStyle(fontSize: 22),
                    ),
                  ),
                  Container(
                    height: 20,
                  ),
                  Container(
                      padding: const EdgeInsets.only(top: 0),
                      height: 200,
                      child: Scrollbar(
                          child: ListView.builder(
                              padding: const EdgeInsets.only(top: 10),
                              itemCount: statuses.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  child: CheckboxListTile(
                                      controlAffinity:
                                          ListTileControlAffinity.leading,
                                      title: Text(statuses[index].key),
                                      value: statuses[index].value,
                                      onChanged: (val) {
                                        setState(() {
                                          statuses.forEach((element) {
                                            element.value = false;
                                          });

                                          if (val != null) {
                                            statuses[index]!.value = val;
                                            status = statuses[index]!.key;
                                          }
                                        });
                                      }),
                                );
                              }))),
                  Container(height: 30),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Rooms:",
                            style: TextStyle(fontSize: 22),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 50,
                        child: RawMaterialButton(
                          child: Icon(Icons.add),
                          onPressed: () {
                            setState(() {
                              numOfRows++;
                              roomListHeight = (80 * numOfRows).toDouble();
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        width: 50,
                        child: RawMaterialButton(
                          child: Icon(Icons.remove),
                          onPressed: () {
                            setState(() {
                              if (numOfRows > 1) {
                                if (rooms.length == numOfRows) {
                                  rooms.removeLast();
                                }

                                numOfRows--;
                                roomListHeight = (80 * numOfRows).toDouble();
                              }
                            });
                          },
                        ),
                      )
                    ],
                  ),
                  Container(
                    // color: Colors.red,
                    height: roomListHeight,
                    child: ListView.builder(
                        itemCount: numOfRows,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 20),
                              child: Row(
                                children: [
                                  Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(right: 15),
                                        child: TextFormField(
                                          controller: TextEditingController()
                                            ..text = index < rooms.length
                                                ? rooms[index].name
                                                : "",
                                          onChanged: (text) {
                                            if (rooms.length <= index) {
                                              Room a = Room();
                                              a.name = text;
                                              rooms.add(a);
                                            } else {
                                              rooms[index].name = text;
                                            }
                                          },
                                          validator: (value) {
                                            if (value == null ||
                                                value.isEmpty) {
                                              return 'Required field';
                                            }
                                            return null;
                                          },
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(),
                                            labelText: 'Room Name',
                                          ),
                                        ),
                                      )),
                                  Expanded(
                                    child: TextFormField(
                                      controller: TextEditingController()
                                        ..text = index < rooms.length
                                            ? rooms[index].quantity.toString()
                                            : "",
                                      onChanged: (text) {
                                        if (index > rooms.length - 1) {
                                          Room a = Room();
                                          a.quantity = int.parse(text);
                                          rooms.add(a);
                                        } else {
                                          if (text.length > 0) {
                                            rooms[index].quantity =
                                                int.parse(text);
                                          }
                                        }
                                      },
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Required field';
                                        }
                                        return null;
                                      },
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Quantity',
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                  Container(height: 20),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Notes:",
                      style: TextStyle(fontSize: 22),
                    ),
                  ),
                  Container(height: 20),
                  TextFormField(
                    controller: TextEditingController()..text = note,
                    textInputAction: TextInputAction.newline,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0)),
                      // labelText: 'Reason for visit*',
                      alignLabelWithHint: true,
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      hintText: 'Add your note here',
                    ),
                    minLines: 4, // initial height
                    /// If maxLines is set to null, there is no limit to the number of lines, and the wrap is enabled. The field sizes itself to the inner text
                    maxLines: 6,
                    maxLength: 300,
                    onChanged: (text) {
                      note = text;
                    },
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: ElevatedButton(
                            onPressed: () async {
                              // Validate returns true if the form is valid, or false otherwise.
                              if (_formKey.currentState!.validate()) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text('Processing Data')),
                                );
                              }
                              await submit();
                            },
                            child: const Text('Save changes'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0),
                        child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.grey)),
                          onPressed: () async {
                            showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                      title: const Text('Confirm'),
                                      content: SingleChildScrollView(
                                        child: ListBody(
                                          children: <Widget>[
                                            Column(
                                              children: [
                                                Row(
                                                  children: [
                                                    Flexible(
                                                      child: Text(
                                                          "Do you want to delete this property?"),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                      actions: <Widget>[
                                        TextButton(
                                          onPressed: () =>
                                              Navigator.pop(context, 'Cancel'),
                                          child: const Text('Cancel'),
                                        ),
                                        TextButton(
                                          onPressed: () async {
                                            var a = await GraphQLClass.shared
                                                .clientToQuery()
                                                .mutate(MutationOptions(
                                                    document: gql(APIEndpoint
                                                        .deleteProperty),
                                                    variables: {'id': id}));
                                            print(a);
                                            await _showMyDialog(
                                                "Success", "Deleted property");
                                            Navigator.pop(context);
                                            Navigator.pop(context, 'Delete');
                                          },
                                          child: const Text('Delete'),
                                        ),
                                      ],
                                    ));
                          },
                          child: Icon(Icons.delete),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )),
          ),
        ));
  }
}
