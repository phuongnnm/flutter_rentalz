import 'package:flutter/material.dart';
import 'package:flutter_rentalz/models/room.dart';
import 'package:flutter_rentalz/network/endpoints.dart';
import 'package:flutter_rentalz/network/network.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class CustomForm extends StatefulWidget {
  const CustomForm({
    Key? key,
  }) : super(key: key);

  @override
  _CustomFormState createState() => _CustomFormState();
}

class TypeCheckValue {
  String key;
  bool value;

  TypeCheckValue({required this.key, required this.value});
}

class _CustomFormState extends State<CustomForm> {
  final _formKey = GlobalKey<FormState>();
  late bool isChecked = false;
  Map<String, bool> values = {
    'foo': false,
    'bar': false,
    'nah': false,
  };

  List arr = [
    TypeCheckValue(key: 'MANSION', value: false),
    TypeCheckValue(key: 'HOUSE', value: false),
    TypeCheckValue(key: 'BUNGALOW', value: false)
  ];

  List statuses = [
    TypeCheckValue(key: 'FURNISHED', value: false),
    TypeCheckValue(key: 'UNFURNISHED', value: false),
    TypeCheckValue(key: 'PARTFURNISHED', value: false)
  ];

  String? rName;
  String? type;
  String? status;
  double? price;
  double roomListHeight = 80;
  List rooms = [];

  @override
  initState() {
    super.initState();
  }

  int numOfRows = 1;
  bool isSubmit = false;

  Future<void> _showMyDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> submit() async {
    if (rName != null && type != null && status != null && rooms.length > 0) {
      if (rName!.length > 1) {
        // var iPrice = double.parse(price);
        var a = await GraphQLClass.shared.clientToQuery().mutate(
                MutationOptions(
                    document: gql(APIEndpoint.registerProperty),
                    variables: {
                  'input': {
                    'furnishStatus': status,
                    'type': type,
                    'reporterName': rName,
                    'rooms': rooms,
                    'price': price
                  }
                }));
        if (a.data != null) {
          await _showMyDialog("Success", "Your property has been added.");
        } else {
          await _showMyDialog("Error", "An error occured.");
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Scaffold(
          appBar: AppBar(
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.arrow_back_ios),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                );
              },
            ),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 50, left: 15, right: 15, bottom: 25),
              child: Column(
                children: [
                  Center(
                    child: Text(
                      "Add a property",
                      style: TextStyle(fontSize: 35),
                    ),
                  ),
                  Container(
                    height: 35,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Your name*:",
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                  Container(
                    height: 20,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 1.0),
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      hintText: 'Enter your name',
                    ),
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    onChanged: (text) {
                      rName = text;
                    },
                  ),
                  Container(
                    height: 35,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Property type*:",
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                  Container(
                      padding: const EdgeInsets.only(top: 10),
                      height: 190,
                      child: Scrollbar(
                          child: ListView.builder(
                        padding: const EdgeInsets.only(top: 10),
                        itemCount: arr.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            child: CheckboxListTile(
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                title: Text(arr[index].key),
                                value: arr[index].value,
                                onChanged: (val) {
                                  setState(() {
                                    arr.forEach((element) {
                                      element.value = false;
                                    });

                                    arr[index]!.value = val;
                                    isSubmit = val!;
                                    type = arr[index].key;
                                  });
                                }),
                          );
                        },
                      ))),
                  Container(
                    height: 30,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Furniture type:",
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                  Container(
                    height: 20,
                  ),
                  Container(
                      padding: const EdgeInsets.only(top: 0),
                      height: 190,
                      child: Scrollbar(
                          child: ListView.builder(
                        padding: const EdgeInsets.only(top: 10),
                        itemCount: statuses.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            child: CheckboxListTile(
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                title: Text(statuses[index].key),
                                value: statuses[index].value,
                                onChanged: (val) {
                                  setState(() {
                                    statuses.forEach((element) {
                                      element.value = false;
                                    });

                                    if (val != null) {
                                      status = statuses[index].key;
                                      statuses[index]!.value = val;
                                    }
                                  });
                                }),
                          );
                        },
                      ))),
                  Container(
                    height: 30,
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Rooms*:",
                            style: TextStyle(fontSize: 30),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 50,
                        child: RawMaterialButton(
                          child: Icon(Icons.add),
                          onPressed: () {
                            setState(() {
                              numOfRows++;
                              roomListHeight = (80 * numOfRows).toDouble();
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        width: 50,
                        child: RawMaterialButton(
                          child: Icon(Icons.remove),
                          onPressed: () {
                            setState(() {
                              if (numOfRows > 1) {
                                if (rooms.length == numOfRows) {
                                  rooms.removeLast();
                                }
                                numOfRows--;
                                roomListHeight = (80 * numOfRows).toDouble();
                              }
                            });
                          },
                        ),
                      )
                    ],
                  ),
                  Container(
                    height: 20,
                  ),
                  Container(
                    // color: Colors.red,
                    height: roomListHeight,
                    child: ListView.builder(
                        itemCount: numOfRows,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 20),
                              child: Row(
                                children: [
                                  Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(right: 15),
                                        child: TextFormField(
                                          onChanged: (text) {
                                            if (rooms.length <= index) {
                                              Room a = Room();
                                              a.name = text;
                                              rooms.add(a);
                                            } else {
                                              rooms[index].name = text;
                                            }
                                          },
                                          validator: (value) {
                                            if (value == null ||
                                                value.isEmpty) {
                                              return 'Required field';
                                            }
                                            return null;
                                          },
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(),
                                            labelText: 'Room Name',
                                          ),
                                        ),
                                      )),
                                  Expanded(
                                    child: TextFormField(
                                      onChanged: (text) {
                                        if (rooms.length <= index) {
                                          Room a = Room();
                                          a.quantity = int.parse(text);
                                          rooms.add(a);
                                        } else {
                                          rooms[index].quantity =
                                              int.parse(text);
                                        }
                                      },
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Required field';
                                        }
                                        return null;
                                      },
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Quantity',
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                  Container(
                    height: 30,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Price* (VND):",
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                  Container(
                    height: 20,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 1.0),
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      hintText: 'Enter property\'s price',
                    ),
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    onChanged: (text) {
                      price = double.parse(text);
                    },
                  ),
                  Container(
                    height: 30,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Notes:",
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                  Container(
                    height: 20,
                  ),
                  TextFormField(
                    textInputAction: TextInputAction.newline,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0)),
                      // labelText: 'Reason for visit*',
                      alignLabelWithHint: true,
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      hintText: 'Add your additional notes here',
                    ),
                    minLines: 4, // initial height
                    /// If maxLines is set to null, there is no limit to the number of lines, and the wrap is enabled. The field sizes itself to the inner text
                    maxLines: 6,
                    maxLength: 300,
                    onChanged: (text) {
                      print(text);
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      onPressed: isSubmit
                          ? () {
                              if (_formKey.currentState!.validate()) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text('Processing Data')),
                                );
                              }
                              if (rName != null &&
                                  rooms.length > 0 &&
                                  price != null) {
                                showDialog<String>(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        AlertDialog(
                                          title:
                                              const Text('Confirm your info'),
                                          content: SingleChildScrollView(
                                            child: ListBody(
                                              children: <Widget>[
                                                Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Text("Name: "),
                                                        Text(rName!)
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        Text("Property Type: "),
                                                        Text(type!)
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        Text("Price: "),
                                                        Text(price!.toString())
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        Text(
                                                            "Furnish Status: "),
                                                        Text(status ?? "null")
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        Text("Rooms: "),
                                                      ],
                                                    ),
                                                    Container(
                                                      height: 80,
                                                      width: 200,
                                                      child: ListView.builder(
                                                          itemCount:
                                                              rooms.length,
                                                          itemBuilder:
                                                              (BuildContext
                                                                      context,
                                                                  int index) {
                                                            return Container(
                                                              child: Row(
                                                                children: [
                                                                  Expanded(
                                                                      child: Text(
                                                                          'Name: ${rooms[index].name} \n Quantity: ${rooms[index].quantity}'))
                                                                ],
                                                              ),
                                                            );
                                                          }),
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                          actions: <Widget>[
                                            TextButton(
                                              onPressed: () => Navigator.pop(
                                                  context, 'Cancel'),
                                              child: const Text('Cancel'),
                                            ),
                                            TextButton(
                                              onPressed: () async {
                                                await submit();
                                                Navigator.pop(
                                                    context, 'Submit');
                                              },
                                              child: const Text('Submit'),
                                            ),
                                          ],
                                        ));
                              }
                            }
                          : null,
                      child: const Text('Submit'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
