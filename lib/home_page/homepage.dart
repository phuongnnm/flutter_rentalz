import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rentalz/home_page/detailPage.dart';
import 'package:flutter_rentalz/models/property-info.dart';
import 'package:flutter_rentalz/network/endpoints.dart';
import 'package:flutter_rentalz/util.dart';
import 'package:flutter_rentalz/values/app-colors.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter_rentalz/network/network.dart';
import 'package:graphql/client.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  bool val = false;
  List props = [];
  String searchString = "";

  Widget renderUI(List data) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 10),
          height: 550,
          child: Scrollbar(
            child: ListView.builder(
              itemCount: data.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                    padding: const EdgeInsets.all(20),
                    child: GestureDetector(
                        onTap: () {
                          setState(() {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) =>
                                        DetailPage(id: data[index].id)));
                          });
                        },
                        child: Container(
                          height: 100,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: AppColor.darkGray.withOpacity(0.4),
                                  blurRadius: 8,
                                  spreadRadius: 2,
                                  offset: Offset(4, 4),
                                ),
                              ],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12))),
                          child: Container(
                            padding: const EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Icon(Icons.home),
                                Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            data[index].createdAt ?? "null",
                                            style: TextStyle(fontSize: 15),
                                          ),
                                        ),
                                        Expanded(
                                            child: Text(
                                                data[index].propertyType,
                                                style:
                                                    TextStyle(fontSize: 19))),
                                        Expanded(
                                            child: RichText(
                                          text: TextSpan(
                                            text: 'Reported by: ',
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.black),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text:
                                                      data[index].reporterName,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ],
                                          ),
                                        )),
                                      ],
                                    ))
                              ],
                            ),
                          ),
                        )));
              },
            ),
          ),
        )
      ],
    );
    // );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Search Page"),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
            );
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
                padding: const EdgeInsets.all(15),
                child: Row(children: [
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Enter a name or a property type',
                      ),
                      onChanged: (text) {
                        searchString = text;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: ElevatedButton(
                        onPressed: () async {
                          var a = await GraphQLClass.shared
                              .clientToQuery()
                              .query(QueryOptions(
                                  document:
                                      gql(APIEndpoint.searchForPropByKeyWord),
                                  variables: {'keyword': searchString}));
                          List res = a.data!['searchForPropByKeyWord'];
                          List prop2 = [];

                          res.forEach((element) {
                            var date = Util.convertDate(element['createdAt']);
                            PropertyInfo e = PropertyInfo(
                                reporterName: element['reporterName'],
                                propertyType: element['type'],
                                id: element['id'],
                                status: element['furnishStatus'],
                                createdAt: date);
                            prop2.add(e);
                          });
                          setState(() {
                            props = prop2;
                          });
                        },
                        child: const Text('Search')),
                  )
                ])),
            Container(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 20, top: 20),
                child: Row(children: [
                  Expanded(
                    flex: 3,
                    child: Text(
                      "Properties",
                      style: TextStyle(fontSize: 22),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: RawMaterialButton(
                      onPressed: () async {
                        var a = await GraphQLClass.shared
                            .clientToQuery()
                            .query(QueryOptions(
                              document: gql(APIEndpoint.getAllProps),
                            ));
                        print(a);
                        if (a.data == null) {
                          return;
                        }
                        List res = a.data!['getAllProperties'];
                        List prop2 = [];

                        res.forEach((element) {
                          print(element['id']);
                          String date = Util.convertDate(element['createdAt']);
                          print(date);
                          PropertyInfo e = PropertyInfo(
                              reporterName: element['reporterName'],
                              propertyType: element['type'],
                              id: element['id'],
                              status: element['furnishStatus'],
                              createdAt: date);
                          prop2.add(e);
                        });
                        setState(() {
                          props = prop2;
                        });
                        print(res);
                      },
                      child: Text(
                        "Show all properties",
                        style: TextStyle(color: AppColor.purple),
                      ),
                    ),
                  )
                ]),
              ),
            ),
            SizedBox(
              height: 550,
              child: ListView.builder(
                  itemCount: props.length,
                  itemBuilder: (context, index) {
                    return renderUI(props);
                  }),
            )
          ],
        ),
      ),
    );
  }
}
